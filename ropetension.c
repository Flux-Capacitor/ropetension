/*ropetension.c
 * Version 0.6.1  / 20.02.2019
 * Copyright 2019 sven.rzesnik@protonmail.com
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
/*Programm zur Berechnung der Seillast bei bestimmten Winkeln
 * Compile with gcc -Wall -Werror -std=c99 ropetension.c -o ropetension -lm
* Start with ./ropetension
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include <ctype.h>

//#define _USE_MATH_DEFINES
const double PI  = 3.1415926535898;

double LENGTH, HEIGHT;
double rope_angle(double LENGTH, double HEIGHT) { // Seilwinkel berechnen / compute rope angle
	double length_half, resultsw, x, rad2deg;
	length_half = LENGTH / 2.0;
	rad2deg = 180.0 / PI; // Bogenmass in Grad umrechnen / degree instead of radian
	x = length_half / HEIGHT;
	resultsw = atan(x) * rad2deg;
	return resultsw;
	}

double WEIGHT_FORCE;
double ropetension(double WEIGHT_FORCE) { // Seilkraft berechnen / compute rope tension
	double weight_force_per_rope, resultsk, x, rad2deg;
	weight_force_per_rope = WEIGHT_FORCE / 2.0;
	rad2deg = PI / 180.0; // Bogenmass in Grad umrechnen / degree instead of radian
	x = cos(rope_angle(LENGTH, HEIGHT) * rad2deg);
	resultsk = weight_force_per_rope / x;
	return resultsk;
	}

int main(void){
	char tmp = '0';
	int check;
	printf("\nBerechnung der Seillast bei bestimmten Winkeln.\n");
	printf("***********************************************\n\n");
	printf("\nBitte die Laenge eingeben: ");
	
	check = scanf("%lf", &LENGTH);
		if (check ==1 ) { 
		printf("OK\n");
   }
   else {
		printf("Sie haben keine Zahl eingegeben - Abbruch\n");
   return EXIT_FAILURE;
   }
	
	printf("\nBitte die Hoehe eingeben: ");
	check = scanf("%lf", &HEIGHT);
		if (check ==1 ) {
		printf("OK\n");
   }
   else {
		printf("Sie haben keine Zahl eingegeben - Abbruch\n");
   return EXIT_FAILURE;
   }
	printf("\nWinkel=%lf\n\n", rope_angle(LENGTH, HEIGHT));
	if (rope_angle(LENGTH, HEIGHT) > 60.0) {
		printf("ACHTUNG - UNFALLGEFAHR!\nDer Winkel darf laut DGUV Information 209-021 der BGHM\nnicht groesser als 60 Grad sein");
	}
	printf("\nBitte das Gewicht eingeben: ");
	check = scanf("%lf", &WEIGHT_FORCE);
		if (check ==1 ) {
		printf("OK\n");
   }
   else {
		printf("Sie haben keine Zahl eingegeben - Abbruch\n");
   return EXIT_FAILURE;
   }
	printf("\nSeillast=%lf\n", ropetension(WEIGHT_FORCE));
	printf("===========================\n\n");
	scanf("%c", &tmp);
	scanf("%c", &tmp);
return EXIT_SUCCESS;
}
